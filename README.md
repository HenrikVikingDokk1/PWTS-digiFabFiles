# Playing with the Sun Construction Kit Digital Fabrication Files

This repository contains digital files necessary to fabricate 3D printed or lasercut elements of the [Playing with the Sun Construction Kit](https://docs.playingwiththesun.org/Const-Kit-Overview/). 

Each element lives in its own folder, along with a README.md that explains how to cut / print it. 

Happy fabricating!